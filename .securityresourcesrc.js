'use strict';

module.exports = {
  resources: [
    { code: 'RESOURCE', service: 'harmony-service', name: 'Resource Full Access', uri: 'Resouce:*', allowedRights: ['READ', 'INSERT', 'UPDATE'] }
  ]
};
