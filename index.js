'use strict';

const { config } = require('exframe-configuration');
const service = require('exframe-service');
const app = require('exframe-rest').create(config.default.service.port);
const logger = require('./lib/logger');
const pkg = require('./package.json');
const health = require('./lib/health-service');

require('./lib/db');

service.init({ logger, timeout: config.default.service.gracefulShutdownTimeout });

app.use('/health', health.checkHealth);

logger.info(`Harmony service started: version ${pkg.version}`);
logger.info(`Listening for requests on port ${config.default.service.port}`);
