'use strict';

const { resources } = require('../.securityresourcesrc');

const resourceFullAccess = resources.find(x => x.code === 'RESOURCE');

const buildCspFilter = (context, resourceUri, ...rights) => ({
  $or: context.user.userSecurityResources
    .filter(resource => resource.resourceUri === resourceUri && rights.includes(resource.right))
    .map(({ companyCode, state, product }) => ({
      companyCode, state, product
    }))
});

const buildResourceCspFilter = (context, ...rights) => buildCspFilter(context, resourceFullAccess.uri, ...rights);

module.exports = {
  buildResourceCspFilter
};
