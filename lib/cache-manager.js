'use strict';

const { config: { default: config } } = require('exframe-configuration');
const cacheManager = require('exframe-cache-manager').create({ ...config.redis });

module.exports = cacheManager;
