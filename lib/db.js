'use strict';

const { config } = require('exframe-configuration');
const { init } = require('exframe-db');
const logger = require('./logger');

const db = init({ logger, name: 'harmony', dbUrl: config.default.mongo.harmony.url });

const ready = db.connect()
  .then(
    () => true,
    error => {
      logger.error('Failed to connect to the harmony database', { errorData: error });
      throw error;
    }
  );

module.exports = { ready, db, mongoose: db.mongoose };
