'use strict';

const health = require('exframe-health');

module.exports = {
  checkHealth: health.check
};
