'use strict';

const logger = require('exframe-logger').create(process.env.LOGSENE_TOKEN);

module.exports = logger;
