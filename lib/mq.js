'use strict';

const config = require('exframe-configuration').config.default;
const mq = require('exframe-mq');

const logger = require('./logger');

const instance = mq.create({ logger, ...config.messaging });
const client = instance.client({ eventing: config.messaging.eventing });

module.exports = { client, instance };
