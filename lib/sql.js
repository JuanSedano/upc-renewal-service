'use strict';

const knex = require('knex');
const { config: { sql: { host, user, password, db, port } } } = require('exframe-configuration');
const logger = require('./logger');

const options = {
  client: 'sql',
  connection: {
    host, user, password, db, port
  },
  pool: {
    afterCreate: (conn, done) => {
      logger.info('created new sql connection pool');

      conn.on('error', (err) => {
        logger.error('sql connection failed', err);
      });

      done();
    }
  }
};

const sql = knex(options);

module.exports = { sql };
