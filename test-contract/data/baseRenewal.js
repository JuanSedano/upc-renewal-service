'use strict';

const moment = require('moment-timezone');
const { randomInt } = require('../support/common');

let sequenceNumber = Date.now() % 10000000;
const prefix = randomInt(10, 99);

const getPolicyNumber = () => `${prefix}-${String(sequenceNumber++).padStart(7, '0')}-02`; // eslint-disable-line no-plusplus

const get = () => {
  const policyNumber = getPolicyNumber();
  const effectiveDate = moment.tz('America/New_York').startOf('day').format('YYYY-MM-DDTHH:mm:ss.SSSZ');
  const endDate = moment(effectiveDate).add(1, 'years');
  return {
    policyNumber,
    policyTerm: 2,
    policyAccountCode: 10000,
    sourceNumber: '12-4003156-01',
    effectiveDate,
    endDate,
    cancelDate: null,
    cancelReason: null,
    agencyCode: 20003,
    agentCode: 60586,
    billToId: '59cbffdc85fe850012fd7870',
    billToType: 'Additional Interest',
    billPlan: 'Annual',
    property: {
      windMitigation: {
        roofGeometry: 'Other',
        floridaBuildingCodeWindSpeed: 120,
        secondaryWaterResistance: 'Other',
        internalPressureDesign: 'Other',
        roofCovering: 'FBC',
        openingProtection: 'Other',
        terrain: 'B',
        floridaBuildingCodeWindSpeedDesign: 120,
        roofDeckAttachment: 'A',
        windBorneDebrisRegion: 'Other',
        roofToWallConnection: 'Clips'
      },
      floodZone: 'X',
      source: 'CasaClue',
      squareFeet: 2309,
      residenceType: 'SINGLE FAMILY',
      distanceToTidalWater: 128779.2,
      buildingCodeEffectivenessGrading: 99,
      familyUnits: '1-2',
      constructionType: 'MASONRY',
      distanceToFireStation: 1.49,
      id: '121172F86891E7A77',
      yearBuilt: 1990,
      territory: '512-0',
      sprinkler: 'N',
      yearOfRoof: null,
      physicalAddress: {
        city: 'WINTER SPRINGS',
        latitude: 28.67859,
        zip: '32708',
        state: 'FL',
        address2: '',
        longitude: -81.23743,
        county: 'SEMINOLE',
        address1: '743 KENTSTOWN CT'
      },
      distanceToFireHydrant: 1000,
      protectionClass: 5,
      gatedCommunity: false,
      burglarAlarm: false,
      fireAlarm: false,
      trampoline: false,
      divingBoard: false,
      poolSecured: true,
      pool: true,
      townhouseRowhouse: false
    },
    companyCode: 'TTIC',
    state: 'FL',
    product: 'HO3',
    policyHolders: [
      {
        _id: '59c5611727de3700123ac79d',
        emailAddress: 'test@typtap.com',
        primaryPhoneNumber: '4076209892',
        lastName: 'Booth',
        firstName: 'Kelly',
        entityType: 'Person',
        order: 0,
        electronicDelivery: false
      },
      {
        _id: '59cbfe6085fe850012fd75d5',
        emailAddress: 'test@typtap.com',
        primaryPhoneNumber: '4076209892',
        lastName: 'Booth Jr. ',
        firstName: 'Michael',
        entityType: 'Person',
        order: 1,
        electronicDelivery: false
      }
    ],
    coverageLimits: {
      personalProperty: {
        format: 'Currency',
        amount: 81250,
        letterDesignation: 'C',
        displayText: 'Personal Property'
      },
      otherStructures: {
        format: 'Currency',
        amount: 16250,
        letterDesignation: 'B',
        displayText: 'Other Structures'
      },
      medicalPayments: {
        format: 'Currency',
        amount: 2000,
        letterDesignation: 'F',
        displayText: 'Medical Payments'
      },
      moldProperty: {
        amount: 10000,
        displayText: 'Mold Property',
        format: 'Currency'
      },
      ordinanceOrLaw: {
        format: 'Percentage',
        amount: 25,
        displayText: 'Ordinance or Law',
        ofCoverageLimit: 'dwelling'
      },
      lossOfUse: {
        format: 'Currency',
        amount: 32500,
        letterDesignation: 'D',
        displayText: 'Loss of Use'
      },
      personalLiability: {
        format: 'Currency',
        amount: 300000,
        letterDesignation: 'E',
        displayText: 'Personal Liability'
      },
      dwelling: {
        format: 'Currency',
        maxAmount: 350000,
        amount: 325000,
        letterDesignation: 'A',
        minAmount: 242000,
        displayText: 'Dwelling'
      },
      moldLiability: {
        amount: 50000,
        displayText: 'Mold Liability',
        format: 'Currency'
      }
    },
    coverageOptions: {
      sinkholePerilCoverage: {
        answer: true,
        displayText: 'Sinkhole Peril Coverage'
      },
      propertyIncidentalOccupanciesOtherStructures: {
        answer: false,
        displayText: 'Property Permitted Incidental Occupancies Other Structures'
      },
      liabilityIncidentalOccupancies: {
        answer: false,
        displayText: 'Liability Permitted Incidental Occupancies'
      },
      personalPropertyReplacementCost: {
        answer: true,
        displayText: 'Personal Property Replacement Cost'
      },
      propertyIncidentalOccupanciesMainDwelling: {
        answer: false,
        displayText: 'Property Permitted Incidental Occupancies Main Dwelling'
      }
    },
    deductibles: {
      sinkhole: {
        format: 'Percentage',
        amount: 10,
        displayText: 'Sinkhole',
        ofCoverageLimit: 'dwelling',
        calculatedAmount: 32500
      },
      hurricane: {
        format: 'Percentage',
        amount: 2,
        displayText: 'Hurricane',
        ofCoverageLimit: 'dwelling',
        calculatedAmount: 6500
      },
      allOtherPerils: {
        amount: 2500,
        displayText: 'All Other Perils',
        format: 'Currency',
        ofCoverageLimit: 'dwelling',
        calculatedAmount: 2500
      }
    },
    underwritingAnswers: {
      previousClaims: {
        answer: 'No claims ever filed',
        question: 'How many claims in the past 5 years?',
        source: 'Customer'
      },
      rented: {
        answer: 'Never',
        question: 'Is the home or any structures on the property ever rented?',
        source: 'Customer'
      },
      monthsOccupied: {
        answer: '10+',
        question: 'How many months a year does the owner live in the home?',
        source: 'Customer'
      },
      fourPointUpdates: {
        answer: 'Yes',
        question: 'Has the wiring, plumbing, HVAC, and roof been updated in the last 35 years?',
        source: 'Customer'
      },
      noPriorInsuranceSurcharge: {
        question: 'If not new purchase, please provide proof of prior insurance.',
        answer: 'No',
        source: 'Default'
      },
      floodCoverage: {
        question: 'Does this property have a separate insurance policy covering flood losses?',
        answer: 'Yes',
        source: 'Default'
      },
      business: {
        question: 'Is business conducted on this property?',
        answer: 'Yes',
        source: 'Default'
      }
    },
    cost: {
      worksheet: {
        calculatedFields: {
          hurricaneTEFactor: 1220.6,
          retentionExp: 1512.6,
          coverageAFactor: 1.468,
          catExp: 1034,
          nonCatExp: 1693,
          adminExp: 217
        },
        lookupFields: {
          nonCatConstructionLossCost: 0,
          minCoverageA: 150000,
          exteriorWaterLossCost: 180,
          maxCoverageA: 750000,
          hurricaneOpeningProtectionFactor: 0.77461,
          liabilityLossCost: 10,
          baseCoverageA: 250000,
          hurricaneFactor: 1.864885,
          fireLossCost: 101,
          claimRateFactor: 6.1,
          baseCost: 125,
          sinkholeLossCost: 4,
          theftLossCost: 39,
          hurricaneDeductibleFactor: 0.586234,
          lossCostAdjust: 80,
          countyName: 'MIAMI-DADE',
          hurricaneYearBuiltFactor: 1,
          miscLossCost: 15,
          hurricaneRoofShapeFactor: 1,
          claimCost: 1500,
          hailLossCost: 2,
          otherWindLossCost: 52,
          interiorWaterLossCost: 824,
          hurricaneRetentionMult: 1.463338,
          hurricaneConstructionTypeFactor: 1
        },
        inputFields: {
          product: 'HO3',
          coverageC: 359000,
          hurricaneDeductible: 10,
          openingProtection: 'A',
          replacementCost: true,
          yearBuilt: 1994,
          version: '201801',
          coverageB: 71800,
          constructionType: 'M',
          sinkholeDeductible: 10,
          companyCode: 'TTIC',
          coverageA: 718000,
          state: 'FL',
          zip: '33013',
          roofGeometry: 'Other',
          aopDeductible: 2500,
          coverageD: 71800
        }
      },
      totalCost: 4457
    },
    underwritingExceptions: [
      {
        active: true,
        action: 'Fatal Error',
        category: 'Property',
        displayText: 'Property in Do Not Write List',
        canOverride: false,
        code: '102',
        agentMessage: 'Address is not eligible for a policy based on the underwriting information.',
        overriddenAt: null,
        internalMessage: 'Address is not eligible for a policy based on the underwriting information.',
        overridden: false,
        fields: []
      },
      {
        active: true,
        action: 'Underwriting Review',
        category: 'Zip Code Settings',
        displayText: 'Premium does not meet minimum cost per 100.',
        canOverride: true,
        code: '222',
        agentMessage: 'Net Premium does not meet the minimum developed premium. Premium must be at least $463.74 to qualify.',
        overriddenAt: null,
        internalMessage: 'Net Premium does not meet the minimum developed premium. Premium must be at least $463.74 to qualify.',
        overridden: false,
        fields: []
      },
      {
        active: true,
        action: 'Underwriting Review',
        category: 'Coverages & Deductibles',
        displayText: 'If netPremium < minNetPremium',
        canOverride: true,
        code: '224',
        agentMessage: 'Quoted premium is less than $3,000.00 the minimum amount allowed for policy issuance. Review is required.',
        overriddenAt: null,
        internalMessage: 'Quoted premium is less than $3,000.00 the minimum amount allowed for policy issuance.',
        overridden: false,
        fields: []
      }
    ],
    policyHolderMailingAddress: {
      city: 'WINTER SPRINGS',
      zip: '32708',
      state: 'FL',
      zipExtension: '6100',
      country: {
        code: 'USA',
        displayText: 'United States of America'
      },
      address2: '',
      careOf: '',
      address1: '743 KENTSTOWN CT'
    },
    additionalInterests: [
      {
        _id: '59cbffdc85fe850012fd7870',
        name2: '',
        name1: 'MB Financial Bank, NA ISAOA',
        order: 0,
        type: 'Mortgagee',
        mailingAddress: {
          city: 'Troy',
          zip: '48007',
          state: 'MI',
          country: {
            displayText: 'United States of America',
            code: 'USA'
          },
          address2: '',
          address1: 'PO Box 7059'
        },
        referenceNumber: 'testharmony123',
        active: true
      }
    ],
    rating: {
      totalFees: 27,
      rateCode: 201704,
      engineCode: 'HO3ByPeril',
      worksheet: {
        fees: {
          figaFee: 0,
          fhcfFee: 0,
          mgaPolicyFee: 25,
          empTrustFee: 2,
          citizensFee: 0
        },
        perilPremiums: {
          hurricane: 254,
          sinkhole: 74,
          liability: 41,
          otherWind: 10,
          water: 706,
          allOtherPerils: 210,
          fire: 423
        },
        totalPremium: 1767,
        netPremium: 1740,
        additionalCoverages: {
          otherStructIncLimits: 0,
          propertyIncidentalOccupancies: 0,
          increasedPersonalLiabilityLimit: 22,
          increasedPropertyMoldFungiLimit: 0,
          liabilityIncidentalOccupancies: 0,
          increasedLiabilityMoldFungiLimit: 0
        },
        subtotalPremium: 1740,
        additionalCoveragesSum: 22,
        perilPremiumsSum: 1718,
        minimumPremiumAdjustment: 0,
        bcegAdjustment: 3,
        totalFees: 27,
        elements: {
          protectionClassFactors: {
            protectionClass: 5,
            hurricane: 0.817,
            sinkhole: 0.852,
            liability: 1,
            otherWind: 0.817,
            water: 0.852,
            constructionType: 'Masonry',
            constructionCode: 'M',
            allOtherPerils: 0.852,
            fire: 0.852
          },
          baseRates: {
            hurricane: 613.41,
            sinkhole: 7.47,
            liability: 56.96,
            otherWind: 22.98,
            water: 516.56,
            allOtherPerils: 153.35,
            fire: 309.05
          },
          ageOfHomeByYearFactors: {
            otherWind: 1.06,
            hurricane: 1.06,
            yearBuilt: 1990
          },
          ageOfHomeFactors: {
            ageOfHome: 27,
            sinkhole: 2.629,
            liability: 2.629,
            water: 2.629,
            allOtherPerils: 2.629,
            fire: 2.629
          },
          noPriorInsuranceFactors: {
            hurricane: 1,
            sinkhole: 1,
            liability: 1,
            otherWind: 1,
            water: 1,
            noPriorInsurance: false,
            allOtherPerils: 1,
            fire: 1
          },
          seasonalFactors: {
            hurricane: 1,
            sinkhole: 1,
            liability: 1,
            seasonal: false,
            otherWind: 1,
            water: 1,
            allOtherPerils: 1,
            fire: 1
          },
          fireAlarmAndSprinklerFactors: {
            hurricane: 1,
            sinkhole: 1,
            liability: 1,
            sprinkler: 'N',
            otherWind: 1,
            water: 1,
            allOtherPerils: 1,
            fire: 1,
            fireAlarm: false
          },
          replacementCostFactors: {
            hurricane: 1.125,
            sinkhole: 1.125,
            liability: 1,
            replacementCost: true,
            otherWind: 1.125,
            water: 1.125,
            allOtherPerils: 1.125,
            fire: 1.125
          },
          ordinanceOrLawFactors: {
            hurricane: 1,
            sinkhole: 1,
            liability: 1,
            ordinanceOrLaw: false,
            otherWind: 1,
            water: 1,
            allOtherPerils: 1,
            fire: 1
          },
          coverageAFactors: {
            hurricane: 2.405,
            sinkhole: 2.405,
            liability: 1,
            otherWind: 2.405,
            water: 2.405,
            allOtherPerils: 2.405,
            fire: 2.405
          },
          territoryFactors: {
            hurricane: 0.451,
            group: 8,
            sinkhole: 1.792,
            liability: 0.271,
            minPremium: 0.002,
            otherWind: 0.451,
            water: 0.273,
            code: '512-0',
            allOtherPerils: 0.273,
            fire: 0.273,
            name: 'Seminole'
          },
          coverageBFactors: {
            hurricane: 0.982,
            sinkhole: 0.982,
            liability: 1,
            otherWind: 0.982,
            water: 0.982,
            allOtherPerils: 0.982,
            fire: 0.982
          },
          coverageCFactors: {
            hurricane: 0.85,
            sinkhole: 0.925,
            liability: 1,
            otherWind: 0.925,
            water: 0.925,
            allOtherPerils: 0.925,
            fire: 0.925
          },
          deductibleFactors: {
            hurricane: 0.91,
            exWind: false,
            sinkhole: 1,
            liability: 1,
            hurricaneDeductible: 2,
            otherWind: 0.91,
            water: 0.91,
            allOtherPerils: 0.91,
            fire: 0.91,
            allOtherPerilsDeductible: 2500
          },
          windMitigationFactors: {
            hurricane: 0.51,
            sinkhole: 1,
            liability: 1,
            otherWind: 0.51,
            water: 1,
            allOtherPerils: 1,
            windMitigationDiscount: 0.49,
            fire: 1
          },
          burglarAlarmFactors: {
            hurricane: 1,
            sinkhole: 1,
            liability: 1,
            otherWind: 1,
            water: 1,
            burglarAlarm: false,
            allOtherPerils: 1,
            fire: 1
          },
          townRowHouseFactors: {
            protectionClass: 5,
            hurricane: 1,
            sinkhole: 1,
            liability: 1,
            units: '1-2',
            otherWind: 1,
            water: 1,
            allOtherPerils: 1,
            fire: 1
          },
          bcegFactors: {
            hurricane: 1.01,
            sinkhole: 1,
            liability: 1,
            grade: 99,
            otherWind: 1.01,
            territoryGroup: 8,
            water: 1,
            allOtherPerils: 1,
            fire: 1
          }
        }
      },
      netPremium: 1740,
      totalPremium: 1767,
      windMitigationDiscount: 0
    },
    forms: [
      {
        formName: 'Mailing page',
        formNumber: 'TTIC HO3 Mail Page',
        editionDate: '01 17'
      },
      {
        formName: 'Welcome letter',
        formNumber: 'TTIC HO Welcome',
        editionDate: '01 17'
      },
      {
        formName: 'Privacy Policy',
        formNumber: 'TTIC Privacy',
        editionDate: '01 16'
      },
      {
        formName: 'Policy Jacket',
        formNumber: 'TTIC HO3J',
        editionDate: '01 17'
      }
    ]
  };
};

module.exports = { get, getPolicyNumber };
