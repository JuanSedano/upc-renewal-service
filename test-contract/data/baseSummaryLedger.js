'use strict';

const shortId = require('shortid');

module.exports = {
  get quarterly() {
    return {
      noticeAmountDue: 608.00,
      companyCode: 'TTIC',
      state: 'FL',
      product: 'HO3',
      policyNumber: shortId.generate(),
      policyTerm: 1,
      policyAccountCode: 10000,
      effectiveDate: '2017-09-06T04:00:00.000Z',
      commission: 150,
      commissionOwed: 125,
      commissionPaid: 25,
      commissionDue: 25,
      cashNeeded: 1466.00,
      cashReceived: 0.00,
      balance: 1466.00,
      initialPremium: 1444,
      currentPremium: 1444,
      sumOfEndorsements: 0,
      fullyEarnedFees: 27,
      equityDate: '2017-09-06T04:00:00.000Z',
      equityInvoiceDate: '2017-09-16T04:00:00.000Z',
      paymentPlanFees: 22,
      billToId: '59821ee451c81a00124e6714',
      billPlan: 'Quarterly',
      billToType: 'Policyholder',
      invoiceDate: '2017-09-05T04:00:00.000Z',
      invoiceDueDate: '2017-09-06T04:00:00.000Z',
      nonPaymentNoticeDate: '2017-09-16T04:00:00.000Z',
      nonPaymentNoticeDueDate: '2017-10-06T04:00:00.000Z',
      refundWriteOffDate: null,
      nextActionNeeded: 'Send Non-Payment Notice',
      nextActionDate: '2017-09-16T04:00:00.000Z',
      invoice: {
        quarterly: {
          q4: {
            dueDate: '2018-06-03T04:00:00.000Z',
            amount: 286,
            invoiceDate: '2018-04-19T04:00:00.000Z'
          },
          q3: {
            dueDate: '2018-03-05T05:00:00.000Z',
            amount: 286,
            invoiceDate: '2018-01-19T05:00:00.000Z'
          },
          q2: {
            dueDate: '2017-12-05T05:00:00.000Z',
            amount: 286,
            invoiceDate: '2017-10-21T04:00:00.000Z'
          },
          q1: {
            dueDate: '2017-09-06T04:00:00.000Z',
            amount: 608,
            invoiceDate: '2017-07-23T04:00:00.000Z'
          }
        }
      },
      status: {
        code: 0,
        displayText: 'No Payment Received'
      }
    };
  },

  get semiAnnual() {
    return {
      noticeAmountDue: 1033.00,
      companyCode: 'TTIC',
      state: 'FL',
      product: 'HO3',
      policyNumber: shortId.generate(),
      policyTerm: 1,
      policyAccountCode: 10000,
      effectiveDate: '2017-08-10T04:00:00.000Z',
      commission: 150,
      commissionOwed: 125,
      commissionPaid: 25,
      commissionDue: 25,
      cashNeeded: 1698.00,
      cashReceived: 0.00,
      balance: 1698.00,
      initialPremium: 1682,
      currentPremium: 1682,
      sumOfEndorsements: 0,
      fullyEarnedFees: 27,
      equityDate: '2017-08-10T04:00:00.000Z',
      equityInvoiceDate: '2017-09-16T04:00:00.000Z',
      paymentPlanFees: 16,
      billToId: '59832d6351c81a00124f917d',
      billPlan: 'Semi-Annual',
      billToType: 'Policyholder',
      invoiceDate: '2017-08-03T04:00:00.000Z',
      invoiceDueDate: '2017-08-10T04:00:00.000Z',
      nonPaymentNoticeDate: '2017-08-20T04:00:00.000Z',
      nonPaymentNoticeDueDate: '2017-09-09T04:00:00.000Z',
      refundWriteOffDate: null,
      nextActionNeeded: 'Send Non-Payment Notice',
      nextActionDate: '2017-08-20T04:00:00.000Z',
      invoice: {
        semiAnnual: {
          s2: {
            dueDate: '2018-02-06T05:00:00.000Z',
            amount: 665,
            invoiceDate: '2017-12-23T05:00:00.000Z'
          },
          s1: {
            dueDate: '2017-08-10T04:00:00.000Z',
            amount: 1033,
            invoiceDate: '2017-06-26T04:00:00.000Z'
          }
        }
      },
      status: {
        code: 0,
        displayText: 'No Payment Received'
      }
    };
  },

  get annual() {
    return {
      noticeAmountDue: 4373.00,
      companyCode: 'TTIC',
      state: 'FL',
      product: 'HO3',
      policyNumber: shortId.generate(),
      policyTerm: 1,
      policyAccountCode: 10000,
      effectiveDate: '2017-09-02T04:00:00.000Z',
      commission: 150,
      commissionOwed: 125,
      commissionPaid: 25,
      commissionDue: 25,
      cashNeeded: 4373.00,
      cashReceived: 0.00,
      balance: 4373.00,
      initialPremium: 4373,
      currentPremium: 4373,
      sumOfEndorsements: 0,
      fullyEarnedFees: 27,
      equityDate: '2017-09-02T04:00:00.000Z',
      equityInvoiceDate: '2017-09-16T04:00:00.000Z',
      paymentPlanFees: 0,
      billToId: '5989f61e51c81a00125336bb',
      billPlan: 'Annual',
      billToType: 'Policyholder',
      invoiceDate: '2017-08-21T04:00:00.000Z',
      invoiceDueDate: '2017-09-02T04:00:00.000Z',
      nonPaymentNoticeDate: '2017-09-12T04:00:00.000Z',
      nonPaymentNoticeDueDate: '2017-10-02T04:00:00.000Z',
      refundWriteOffDate: null,
      nextActionNeeded: 'Send Non-Payment Notice',
      nextActionDate: '2017-09-12T04:00:00.000Z',
      invoice: {
        annual: {
          dueDate: '2017-09-02T04:00:00.000Z',
          amount: 4373,
          invoiceDate: '2017-07-19T04:00:00.000Z'
        }
      },
      status: {
        code: 0,
        displayText: 'No Payment Received'
      }
    };
  }
};
