'use strict';

/* eslint-disable array-bracket-spacing */
/* eslint-disable comma-spacing */
/* eslint-disable indent */
/* eslint-disable no-multi-spaces */

module.exports = {
  requestValidation: {
    required: {
      rootLevel: {
        fields: ['field'                  , 'expectedStatus'],
        values: [['companyCode'           , 400],
                ['state'                  , 400],
                ['product'                , 400],
                ['date'                   , 400],
                ['amount'                 , 400],
                ['type'                   , 400],
                ['payees'                 , 400],
                ['note'                   , 200],
                ['topNote'                , 200],
                ['summaryInformation'     , 200],
                ['memo'                   , 200],
                ['mailingAddress'         , 400]
        ]
      },
      payeesLevel: {
        fields: [ 'field'                 , 'expectedStatus'],
        values: [['payees[0].name1'       , 400],
                [ 'payees[0].name2'       , 200],
                [ 'payees[0].entityType'  , 400]]
      },
      summaryInformationLevel: {
        fields: [ 'field'                           , 'expectedStatus'],
        values: [['summaryInformation[0].column1'   , 200],
                [ 'summaryInformation[0].column2'   , 200]]
      },
      mailingAddressLevel: {
        fields: [ 'field'                       , 'expectedStatus'],
        values: [['mailingAddress.careOf'       , 200],
                [ 'mailingAddress.address1'     , 400],
                [ 'mailingAddress.address2'     , 200],
                [ 'mailingAddress.city'         , 400],
                [ 'mailingAddress.state'        , 400],
                [ 'mailingAddress.zip'          , 400],
                [ 'mailingAddress.zipExtension' , 200],
                [ 'mailingAddress.country'      , 400]]
      }
    },
    typeValidation: {
      rootLevel: {
        fields: [  'field'                , 'value'                             , 'expectedType' , 'expectedStatus'],
        values: [[ 'companyCode'          , 'TTIC'                              , 'string'       , 200],
                [  'companyCode'          , 1234                                , 'string'       , 400],
                [  'state'                , 'FL'                                , 'string'       , 200],
                [  'state'                , 8999                                , 'string'       , 400],
                [  'product'              , 'HO3'                               , 'string'       , 200],
                [  'product'              , 896                                 , 'string'       , 400],
                [  'date'                 , '2020-06-23T04:00:00Z'              , 'datetime'     , 200],
                [  'date'                 , 'pineapple'                         , 'datetime'     , 400],
                [  'date'                 , 1233567                             , 'datetime'     , 400],
                [  'referenceNumber'      , 'pineapple'                         , 'string'       , 200],
                [  'referenceNumber'      , 1234567                             , 'string'       , 400],
                [  'amount'               , 1234567                             , 'number'       , 200],
                [  'amount'               , 'pineapple'                         , 'number'       , 400],
                [  'type'                 , 'commission'                        , 'string'       , 200],
                [  'type'                 , 'pineapple'                         , 'string'       , 400],
                [  'type'                 , 1234567                             , 'string'       , 400],
                [  'payees'               , 'pineapple'                         , 'object'       , 400],
                [  'note'                 , 'pineapple'                         , 'string'       , 200],
                [  'note'                 , 1234567                             , 'string'       , 400],
                [  'topNote'              , 'pineapple'                         , 'string'       , 200],
                [  'topNote'              , 1234567                             , 'string'       , 400],
                [  'summaryInformation'   , 'pineapple'                         , 'object'       , 400],
                [  'memo'                 , 'pineapple'                         , 'string'       , 200],
                [  'memo'                 , 1234567                             , 'string'       , 400],
                [  'mailingAddress'       , 'pineapple'                         , 'object'       , 400]
        ]
      },
      payeesLevel: {
        fields: [ 'field'               , 'value'     , 'expectedType'  , 'expectedStatus'],
        values: [['payees[0].name1'     , 'pineapple' , 'string'        , 200],
                [ 'payees[0].name1'     , 1234567     , 'string'        , 400],
                [ 'payees[0].name2'     , 'pineapple' , 'string'        , 200],
                [ 'payees[0].name2'     , 1234567     , 'string'        , 400],
                [ 'payees[0].entityType', 'Company'    , 'string'        , 200],
                [ 'payees[0].entityType', 'pineapple' , 'string'        , 400],
                [ 'payees[0].entityType', 1234567     , 'string'        , 400]]
      },
      summaryInformationLevel: {
        fields: [ 'field'                           , 'value'     , 'expectedType'  , 'expectedStatus'],
        values: [['summaryInformation[0].column1'   , 'pineapple' , 'string'        , 200],
                [ 'summaryInformation[0].column1'   , 1234567     , 'string'        , 400],
                [ 'summaryInformation[0].column2'   , 'pineapple' , 'string'        , 200],
                [ 'summaryInformation[0].column2'   , 1234567     , 'string'        , 400]]
      },
      mailingAddressLevel: {
        fields: [ 'field'                       , 'value'     , 'expectedType'  , 'expectedStatus'],
        values: [['mailingAddress.careOf'       , 'pineapple' , 'string'        , 200],
                [ 'mailingAddress.careOf'       , 1234567     , 'string'        , 400],
                [ 'mailingAddress.address1'     , 'pineapple' , 'string'        , 200],
                [ 'mailingAddress.address1'     , 1234567     , 'string'        , 400],
                [ 'mailingAddress.address2'     , 'pineapple' , 'string'        , 200],
                [ 'mailingAddress.address2'     , 1234567     , 'string'        , 400],
                [ 'mailingAddress.city'         , 'pineapple' , 'string'        , 200],
                [ 'mailingAddress.city'         , 1234567     , 'string'        , 400],
                [ 'mailingAddress.state'        , 'pineapple' , 'string'        , 200],
                [ 'mailingAddress.state'        , 1234567     , 'string'        , 400],
                [ 'mailingAddress.zip'          , 'pineapple' , 'string'        , 200],
                [ 'mailingAddress.zip'          , 1234567     , 'string'        , 400],
                [ 'mailingAddress.zipExtension' , 'pineapple' , 'string'        , 200],
                [ 'mailingAddress.zipExtension' , 1234567     , 'string'        , 400],
                [ 'mailingAddress.country' , 1234567     , 'string'        , 400]]
      }
    }
  }
};
