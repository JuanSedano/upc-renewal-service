/* eslint arrow-body-style: 0 */
/* eslint no-unused-expressions: 0 */
/* eslint no-multi-spaces: 0 */
/* eslint indent: 0 */
/* eslint import/no-extraneous-dependencies: 0 */
/* eslint comma-spacing: 0 */
/* eslint array-bracket-spacing: 0 */

'use strict';

module.exports = {
  requestValidation: {
    required: {
      rootLevel: {
        fields: ['field', 'expectedStatus'],
        values: [
          ['companyCode', 400],
          ['state',       400],
          ['agencyCode',  400],
          ['amount',      400]
        ]
      }
    },
    typeValidation: {
      rootLevel: {
        fields: ['field',           'value',        'expectedType' ,  'expectedStatus'],
        values: [
          ['companyCode',           'TTIC',         'string',         200],
          ['companyCode',           'TTIICC',       'string',         400],
          ['companyCode',           1234,           'string',         400],
          ['state',                 'FL',           'string',         200],
          ['state',                 'FFLL',         'string',         400],
          ['state',                 8999,           'string',         400],
          ['agencyCode',            13234,          'integer',        200],
          ['agencyCode',            132.34,         'integer',        400],
          ['agencyCode',            '13234',        'integer',        400],
          ['agencyCode',            'test string',  'integer',        400],
          ['amount',                75.37,          'number',         200],
          ['amount',                '75.37',        'number',         400],
          ['note',                  'some string',  'string',         200],
          ['note',                  null,           'string',         400],
          ['checkReferenceNumber',  '00301',        'string',         200],
          ['checkReferenceNumber',  301,            'string',         400],
          ['checkReferenceNumber',  '00403',        'string',         200],
          ['checkReferenceNumber',  403,            'string',         400],
          ['checkReferenceNumber',  undefined,      'string',         200]
        ]
      }
    }
  }
};
