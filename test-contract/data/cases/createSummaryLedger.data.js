/* eslint arrow-body-style: 0 */
/* eslint no-unused-expressions: 0 */
/* eslint no-multi-spaces: 0 */
/* eslint indent: 0 */
/* eslint import/no-extraneous-dependencies: 0 */
/* eslint comma-spacing: 0 */
/* eslint array-bracket-spacing: 0 */

'use strict';

const summaryLedgerExamples = require('../baseSummaryLedger');

module.exports = {
  requestValidation: {
    required: {
      rootLevel: {
        fields: [  'field'                  , 'expectedStatus'],
        values: [[ 'companyCode'            , 400],
                  ['state'                  , 400],
                  ['product'                , 400],
                  ['noticeAmountDue'        , 200],
                  ['policyNumber'           , 400],
                  ['policyTerm'             , 400],
                  ['policyAccountCode'      , 400],
                  ['effectiveDate'          , 400],
                  ['cashNeeded'             , 400],
                  ['cashReceived'           , 400],
                  ['balance'                , 400],
                  ['initialPremium'         , 400],
                  ['currentPremium'         , 400],
                  ['sumOfEndorsements'      , 400],
                  ['fullyEarnedFees'        , 400],
                  ['equityDate'             , 400],
                  ['paymentPlanFees'        , 400],
                  ['billToId'               , 400],
                  ['billPlan'               , 400],
                  ['billToType'             , 400],
                  ['invoiceDate'            , 200],
                  ['invoiceDueDate'         , 200],
                  ['nonPaymentNoticeDate'   , 200],
                  ['nonPaymentNoticeDueDate', 200],
                  ['refundWriteOffDate'     , 200],
                  ['nextActionNeeded'       , 200],
                  ['nextActionDate'         , 200],
                  ['invoice'                , 400],
                  ['status'                 , 400]
        ]
      },
      invoiceLevel: {
        fields: [ 'field'      , 'expectedStatus'],
        values: [['dueDate'    , 400],
                [ 'amount'     , 400],
                [ 'invoiceDate', 400]]
      },
      statusLevel: {
        fields: [ 'field'      , 'expectedStatus'],
        values: [['code'       , 400],
                [ 'displayText', 400]]
      }
    },
    typeValidation: {
      rootLevel: {
        fields: [  'field'                , 'value'                             , 'expectedType' , 'expectedStatus'],
        values: [[ 'companyCode'          , 'TTIC'                              , 'string'       , 200],
                [  'companyCode'          , 1234                                , 'string'       , 400],
                [  'state'                , 'FL'                                , 'string'       , 200],
                [  'state'                , 8999                                , 'string'       , 400],
                [  'product'              , 'HO3'                               , 'string'       , 200],
                [  'product'              , 896                                 , 'string'       , 400],
                [  'noticeAmountDue'      , 496.49                              , 'number'       , 200],
                [  'noticeAmountDue'      , '456'                               , 'number'       , 400],
                [  'policyNumber'         , '12345678'                          , 'string'       , 200],
                [  'policyNumber'         , 123456                              , 'string'       , 400],
                [  'policyTerm'           , 123                                 , 'integer'      , 200],
                [  'policyTerm'           , 'abc'                               , 'integer'      , 400],
                [  'policyAccountCode'    , 123345                              , 'integer'      , 200],
                [  'policyAccountCode'    , 'abc'                               , 'integer'      , 400],
                [  'effectiveDate'        , '2018-11-06T00:00:00Z'              , 'datetime'     , 200],
                [  'effectiveDate'        , 'bad string, very bad string'       , 'datetime'     , 400],
                [  'effectiveDate'        , 1233567                             , 'datetime'     , 400],
                [  'cashNeeded'           , 123.44                              , 'number'       , 200],
                [  'cashNeeded'           , '7894'                              , 'number'       , 400],
                [  'cashReceived'         , 456                                 , 'number'       , 200],
                [  'cashReceived'         , '9303'                              , 'number'       , 400],
                [  'balance'              , 50.00                               , 'number'       , 200],
                [  'balance'              , 'the stuff'                         , 'number'       , 400],
                [  'initialPremium'       , 10                                  , 'integer'      , 200],
                [  'initialPremium'       , 'stuff'                             , 'integer'      , 400],
                [  'currentPremium'       , 50                                  , 'integer'      , 200],
                [  'currentPremium'       , '700'                               , 'integer'      , 400],
                [  'sumOfEndorsements'    , 500                                 , 'integer'      , 200],
                [  'sumOfEndorsements'    , 'the'                               , 'integer'      , 400],
                [  'fullyEarnedFees'      , 30                                  , 'integer'      , 200],
                [  'fullyEarnedFees'      , 'asdf'                              , 'integer'      , 400],
                [  'equityDate'           , '2018-11-06T00:00:00Z'              , 'datetime'     , 200],
                [  'equityDate'           , '9000'                              , 'datetime'     , 400],
                [  'paymentPlanFees'      , 600                                 , 'integer'      , 200],
                [  'paymentPlanFees'      , 'asdf'                              , 'integer'      , 400],
                [  'billToId'             , 'yayaya'                            , 'string'       , 200],
                [  'billToId'             , 23234234                            , 'string'       , 400],
                [  'billPlan'             , 'Annual'                            , 'string'       , 200],
                [  'billPlan'             , 234234234                           , 'string'       , 400],
                [  'billToType'           , 'asdfasdf'                          , 'string'       , 200],
                [  'billToType'           , {}                                  , 'string'       , 400],
                [  'invoiceDate'          , '2018-11-06T00:00:00Z'              , 'datetime'     , 200],
                [  'invoiceDate'          , 'asdfasdfasdf'                      , 'datetime'     , 400],
                [  'invoiceDueDate'       , '2018-11-06T00:00:00Z'              , 'datetime'     , 200],
                [  'invoiceDueDate'       , 'asdfasdf'                          , 'datetime'     , 400],
                [  'nonPaymentNoticeDate' , '2018-11-06T00:00:00Z'              , 'datetime'     , 200],
                [  'nonPaymentNoticeDate' , 'asdfasdfads'                       , 'datetime'     , 400],
                [  'refundWriteOffDate'   , '2018-11-06T00:00:00Z'              , 'datetime'     , 200],
                [  'refundWriteOffDate'   , 'asdfasdf'                          , 'datetime'     , 400],
                [  'nextActionNeeded'     , 'asdfasdf'                          , 'string'       , 200],
                [  'nextActionNeeded'     , 13234                               , 'string'       , 400],
                [  'invoice'              , summaryLedgerExamples.annual.invoice, 'object'       , 200],
                [  'invoice'              , 'some string'                       , 'object'       , 400],
                [  'status'               , summaryLedgerExamples.annual.status , 'object'       , 200],
                [  'status'               , 29239                               , 'object'       , 400]
        ]
      },
      invoiceLevel: {
        fields: [ 'field'      , 'value'               , 'expectedType', 'expectedStatus'],
        values: [['dueDate'    , '2018-11-08T00:00:00Z', 'datetime'    , 200],
                [ 'dueDate'    , 'This is not a time'  , 'datetime'    , 400],
                [ 'amount'     , 500000                , 'number'      , 200],
                [ 'amount'     , {}                    , 'number'      , 400],
                [ 'invoiceDate', '2018-11-08T00:00:00Z', 'datetime'    , 200],
                [ 'invoiceDate', []                    , 'datetime'    , 400]]
      },
      statusLevel: {
        fields: [ 'field'      , 'value'        , 'expectedType', 'expectedStatus'],
        values: [['code'       , 309            , 'integer'     , 200],
                [ 'code'       , 'string'       , 'integer'     , 400],
                [ 'displayText', 'some text yah', 'string'      , 200],
                [ 'displayText', 500            , 'string'      , 400]]
      }
    }
  }
};
