// @ts-nocheck

'use strict';

const { expect } = require('chai');
const axios = require('axios');
const { config: { default: { service } } } = require('exframe-configuration');

context('Health', () => {
  describe('GET /health', () => {
    it('Returns 200', () => {
      const options = {
        method: 'GET',
        url: `${service.url}:${service.port}/health`
      };
      return axios(options)
        .then(response => expect(response.status).to.equal(200));
    });
  });
});
