'use strict';

const { axios } = require('exframe-request');
const shortid = require('shortid');
const { config: { default: { security } } } = require('exframe-configuration');
const { createSystemToken } = require('exframe-security');

const putApplication = async () => axios({
  method: 'post',
  url: 'https://mock-auth0:8888/application',
  data: { _id: '01234567890123', name: 'test-application', token: security.applicationToken }
});

const tticH03Resources = [
  { uri: 'TTIC:FL:HO3:Claims:*', right: 'READ' },
  { uri: 'TTIC:FL:HO3:SummaryLedger:summaryledgers:*', right: 'READ' },
  { uri: 'TTIC:FL:HO3:SummaryLedger:summaryledgers:*', right: 'INSERT' },
  { uri: 'TTIC:FL:HO3:SummaryLedger:summaryledgers:*', right: 'UPDATE' },
  { uri: 'TTIC:FL:HO3:HarmonyData:Commissions:*', right: 'READ' },
  { uri: 'TTIC:FL:HO3:HarmonyData:Commissions:*', right: 'INSERT' },
  { uri: 'TTIC:FL:HO3:HarmonyData:Commissions:*', right: 'UPDATE' },
  { uri: 'TTIC:FL:HO3:Renewal:PendingRenewals:*', right: 'READ' },
  { uri: 'TTIC:FL:HO3:Renewal:PendingRenewals:*', right: 'INSERT' },
  { uri: 'TTIC:FL:HO3:Renewal:PendingRenewals:*', right: 'UPDATE' }
];

const tticAF3Resources = [
  { uri: 'TTIC:FL:AF3:Claims:*', right: 'READ' },
  { uri: 'TTIC:FL:AF3:SummaryLedger:summaryledgers:*', right: 'READ' },
  { uri: 'TTIC:FL:AF3:SummaryLedger:summaryledgers:*', right: 'INSERT' },
  { uri: 'TTIC:FL:AF3:SummaryLedger:summaryledgers:*', right: 'UPDATE' },
  { uri: 'TTIC:FL:AF3:Renewal:PendingRenewals:*', right: 'READ' },
  { uri: 'TTIC:FL:AF3:Renewal:PendingRenewals:*', right: 'INSERT' },
  { uri: 'TTIC:FL:AF3:Renewal:PendingRenewals:*', right: 'UPDATE' }
];

const securityUsers = new Map();

securityUsers.set('SYSTEMUSER', {
  userType: 'CSR',
  profileProperties: {},
  securityResources: [],
  sub: 'auth0|SYSTEMUSER|0'
});

securityUsers.set('SYSTEM_USER', {
  userType: 'CSR',
  profileProperties: {},
  securityResources: [],
  applications: [{ id: '01234567890123', name: 'test-application' }]
});

securityUsers.set('TTIC_CSR_HO3', {
  userType: 'CSR',
  profileProperties: {},
  securityResources: tticH03Resources,
  sub: shortid.generate()
});

securityUsers.set('TTIC_CSR_HO3_ALT', {
  userType: 'CSR',
  profileProperties: {},
  securityResources: tticH03Resources,
  sub: shortid.generate()
});

securityUsers.set('TTIC_CSR_AF3', {
  userType: 'CSR',
  profileProperties: {},
  securityResources: tticAF3Resources,
  sub: shortid.generate()
});

securityUsers.set('INVALID_USER', {
  userType: 'TTIC-20003',
  profileProperties: {},
  securityResources: [],
  sub: shortid.generate()
});

// securityUser { userType, securityProfileProperties, securityResources }
const getAccessKeyFromClone = async (securityUserKey) => {
  if (!securityUsers.has(securityUserKey)) return;

  const securityUser = securityUsers.get(securityUserKey);

  const settings = security || {};
  const securityUrl = settings.tokenUrl || 'https://mock-auth0';
  const securityPort = settings.tokenPort || 8888;
  const falconUrl = settings.falconUrl || 'http://falcon';
  const falconPort = settings.falconPort || 8181;

  const mockAuth0Options = {
    method: 'POST',
    url: `${securityUrl}:${securityPort}/profile/clone`,
    data: {
      userType: securityUser.userType,
      profileProperties: Object.assign(securityUser.profileProperties, { sub: securityUser.sub }),
      securityResources: securityUser.securityResources,
      applications: securityUser.applications
    }
  };
  return axios(mockAuth0Options) // Gets ID token
    .then((response) => {
      const falconOptions = {
        method: 'GET',
        headers: {
          authorization: `Bearer ${response.data}`
        },
        url: `${falconUrl}:${falconPort}/accessToken`
      };
      return axios(falconOptions); // Gets access token
    })
    .then(response => response.data);
};

const getSystemToken = async () => {
  // First create the user profile for the built-in system user
  await getAccessKeyFromClone('SYSTEMUSER');

  // Then create internal token for the system user
  return createSystemToken({ log: { info() {} } });
};

module.exports = {
  getAccessKeyFromClone,
  putApplication,
  securityUsers,
  getSystemToken
};
