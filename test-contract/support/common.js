'use strict';

const { axios } = require('exframe-request');
const { config } = require('exframe-configuration');
// const { ready } = require('../../lib/db');
// const schema = require('../../mongoose-schema');
const { getSystemToken, getAccessKeyFromClone } = require('./authorization');

const cleanNumberDecimal = (field) => {
  const result = field.$numberDecimal ? Number(field.$numberDecimal) : Number(field);
  return result;
};

// async function seedDocumentsForTest(collectionName, docs) {
//   try {
//     await ready;
//     return await schema[collectionName].insertMany(docs);
//   } catch (ex) { /* ignore */ }
// }

const randomInt = (low, high) => Math.floor((Math.random() * ((high - low) + 1)) + low);

const restRequest = async (securityUserKey, path, method = 'GET', body = undefined, { useSystemToken } = {}) => {
  let requestOptions;
  if (method === 'GET') {
    requestOptions = {
      method,
      url: `${config.default.service.url}:${config.default.service.port}/${path}`,
      params: body
    };
  }
  else {
    requestOptions = {
      method,
      url: `${config.default.service.url}:${config.default.service.port}/${path}`,
      data: body
    };
  }

  let key;
  if (useSystemToken) {
    key = await getSystemToken();
  } else {
    key = await getAccessKeyFromClone(securityUserKey);
  }

  if (key) requestOptions.headers = { 'harmony-access-key': key, 'harmony-csp': 'TTIC:FL:HO3' };

  return axios(requestOptions).catch(error => error.response);
};

// HTTP calls

module.exports = {
  restRequest,
  cleanNumberDecimal,
  randomInt
  // seedDocumentsForTest
};
